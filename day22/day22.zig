const std = @import("std");
const debug = std.debug;
const fmt = std.fmt;
const heap = std.heap;
const io = std.io;
const math = std.math;
const mem = std.mem;
const testing = std.testing;

const READ_BUFFER_CAPACITY = 256;

pub fn main() !void {
    var gpa = heap.GeneralPurposeAllocator(.{}){};
    defer debug.assert(!gpa.deinit());

    var steps = std.ArrayList(RebootStep).init(gpa.allocator());
    defer steps.deinit();

    var buf: [READ_BUFFER_CAPACITY]u8 = undefined;
    var reader = io.getStdIn().reader();

    while (reader.readUntilDelimiterOrEof(&buf, '\n')) |maybe_line| {
        const line = maybe_line orelse break;
        var line_elems = mem.split(u8, line, " ");

        const on = line_elems.next().?;
        const cuboid = line_elems.next().?;

        var coordinates: [2][3]isize = undefined;
        var current_axis: usize = 0;
        var cuboid_elems = mem.split(u8, cuboid, ",");
        while (cuboid_elems.next()) |cuboid_elem| {
            var axis_range = mem.split(u8, cuboid_elem, "=");
            _ = axis_range.next().?;
            const range = axis_range.next().?;
            var range_elems = mem.split(u8, range, "..");
            coordinates[0][current_axis] = try fmt.parseInt(isize, range_elems.next().?, 10);
            coordinates[1][current_axis] = try fmt.parseInt(isize, range_elems.next().?, 10);
            current_axis += 1;
        }

        try steps.append(.{
            .on = if (mem.eql(u8, on, "on"))
                true
            else if (mem.eql(u8, on, "off"))
                false
            else
                unreachable,
            .region = .{
                .point_min = .{
                    .x = coordinates[0][0],
                    .y = coordinates[0][1],
                    .z = coordinates[0][2],
                },
                .point_max = .{
                    .x = coordinates[1][0],
                    .y = coordinates[1][1],
                    .z = coordinates[1][2],
                },
            },
        });
    } else |err| return err;

    // part 01

    const region = Cuboid{
        .point_min = .{ .x = -50, .y = -50, .z = -50 },
        .point_max = .{ .x = 50, .y = 50, .z = 50 },
    };

    var steps_limited = std.ArrayList(RebootStep).init(gpa.allocator());
    defer steps_limited.deinit();
    for (steps.items) |step| {
        if (region.contains(step.region)) try steps_limited.append(step);
    }

    const nonoverlaps01 = try reboot(steps_limited.items, gpa.allocator());
    defer gpa.allocator().free(nonoverlaps01);

    var vol01: isize = 0;
    for (nonoverlaps01) |nonoverlap| vol01 += nonoverlap.volume();
    debug.print("part 01 {}\n", .{vol01});

    // part 02

    const nonoverlaps02 = try reboot(steps.items, gpa.allocator());
    defer gpa.allocator().free(nonoverlaps02);

    var vol02: isize = 0;
    for (nonoverlaps02) |nonoverlap| vol02 += nonoverlap.volume();
    debug.print("part 02 {}\n", .{vol02});
}

fn reboot(steps: []const RebootStep, allocator: mem.Allocator) ![]Cuboid {
    var nonoverlaps = std.ArrayList(Cuboid).init(allocator);

    var gpa = heap.GeneralPurposeAllocator(.{}){};
    defer debug.assert(!gpa.deinit());

    var next_steps = std.ArrayList(RebootStep).init(gpa.allocator());
    defer next_steps.deinit();

    var step_idx = steps.len;
    while (step_idx > 0) : (step_idx -= 1) {
        const step = steps[step_idx - 1];
        try next_steps.append(step);
    }

    exec_steps: while (next_steps.popOrNull()) |step| {
        for (nonoverlaps.items) |nonoverlap_region, nonoverlap_idx| {
            if (nonoverlap_region.contains(step.region)) {
                // a cuboid in the nonoverlapping set fully contains the new cuboid
                if (!step.on) {
                    // if we are instructed to turn the cubes within the new cuboid off,
                    // we remove the cuboid containing the new cuboid from the nonoverlappings
                    // set and partition it into smaller cuboids that do not overlap with the
                    // new cuboid.
                    const nonoverlap_region_partitions =
                        try nonoverlap_region.nonoverlapPartitions(step.region, gpa.allocator());
                    defer gpa.allocator().free(nonoverlap_region_partitions);
                    _ = nonoverlaps.swapRemove(nonoverlap_idx);
                    try nonoverlaps.appendSlice(nonoverlap_region_partitions);
                }
                // if we are instructed to turn the cubes within the new cuboid on,
                // nothing happens because they are alreay on
                continue :exec_steps;
            } else if (step.region.contains(nonoverlap_region)) {
                // if the new cuboid fully contains a cuboid in the nonoverlapping set,
                // remove the existing cuboid from the nonoverlapping set since its
                // now contained by the new cuboid
                _ = nonoverlaps.swapRemove(nonoverlap_idx);
                // we add the step back into the list of steps so we can compare it againts
                // the remaining cuboids in the nonoverlapping set
                try next_steps.append(step);
                continue :exec_steps;
            } else if (step.region.overlaps(nonoverlap_region)) {
                // the new cuboid overlaps with a cuboid in the nonoverlapping set
                const overlap = step.region.overlappingRegion(nonoverlap_region);
                if (step.on) {
                    // if we are instructed to turn the cubes on, we removed all the cubes
                    // in the overlapping region from the new cuboid and partitions the
                    // remaining cubes into subcuboids
                    const nonoverlap_region_partitions =
                        try step.region.nonoverlapPartitions(overlap, gpa.allocator());
                    defer gpa.allocator().free(nonoverlap_region_partitions);
                    for (nonoverlap_region_partitions) |part| {
                        try next_steps.append(.{ .on = step.on, .region = part });
                    }
                } else {
                    // if we are instructed to turn the cubes off, we removed all the cubes
                    // in the overlapping region from the cuboid in the nonoverlapping set
                    // and partitions the remaining cubes into subcuboids
                    const nonoverlap_region_partitions =
                        try nonoverlap_region.nonoverlapPartitions(overlap, gpa.allocator());
                    defer gpa.allocator().free(nonoverlap_region_partitions);
                    _ = nonoverlaps.swapRemove(nonoverlap_idx);
                    try nonoverlaps.appendSlice(nonoverlap_region_partitions);
                    // we add the step back into the list of steps so we can compare it againts
                    // the remaining cuboids in the nonoverlapping set
                    try next_steps.append(step);
                }
                continue :exec_steps;
            }
        }
        if (step.on) try nonoverlaps.append(step.region);
    }

    return nonoverlaps.toOwnedSlice();
}

const Point = struct { x: isize, y: isize, z: isize };

const Cuboid = struct {
    const Self = @This();

    point_min: Point,
    point_max: Point,

    fn contains(lhs: Self, rhs: Self) bool {
        const contained_x = lhs.point_min.x <= rhs.point_min.x and lhs.point_max.x >= rhs.point_max.x;
        const contained_y = lhs.point_min.y <= rhs.point_min.y and lhs.point_max.y >= rhs.point_max.y;
        const contained_z = lhs.point_min.z <= rhs.point_min.z and lhs.point_max.z >= rhs.point_max.z;
        const contained = contained_x and contained_y and contained_z;
        return contained;
    }

    fn overlaps(lhs: Self, rhs: Self) bool {
        const overlap_x = lhs.point_min.x <= rhs.point_max.x and lhs.point_max.x >= rhs.point_min.x;
        const overlap_y = lhs.point_min.y <= rhs.point_max.y and lhs.point_max.y >= rhs.point_min.y;
        const overlap_z = lhs.point_min.z <= rhs.point_max.z and lhs.point_max.z >= rhs.point_min.z;
        const overlapped = overlap_x and overlap_y and overlap_z;
        return overlapped;
    }

    fn overlappingRegion(lhs: Self, rhs: Self) Self {
        const region = Cuboid{
            .point_min = .{
                .x = math.max(lhs.point_min.x, rhs.point_min.x),
                .y = math.max(lhs.point_min.y, rhs.point_min.y),
                .z = math.max(lhs.point_min.z, rhs.point_min.z),
            },
            .point_max = .{
                .x = math.min(lhs.point_max.x, rhs.point_max.x),
                .y = math.min(lhs.point_max.y, rhs.point_max.y),
                .z = math.min(lhs.point_max.z, rhs.point_max.z),
            },
        };
        return region;
    }

    fn volume(self: Self) isize {
        const x = self.point_max.x - self.point_min.x + 1;
        const y = self.point_max.y - self.point_min.y + 1;
        const z = self.point_max.z - self.point_min.z + 1;
        return x * y * z;
    }

    fn nonoverlapPartitions(self: Self, overlap_region: Self, allocator: mem.Allocator) ![]Cuboid {
        var cuboids = std.ArrayList(Cuboid).init(allocator);
        errdefer cuboids.deinit();

        const MinMax = struct { min: isize, max: isize };
        var xs: [3]MinMax = undefined;
        var xs_len: usize = 0;
        var ys: [3]MinMax = undefined;
        var ys_len: usize = 0;
        var zs: [3]MinMax = undefined;
        var zs_len: usize = 0;

        xs[0] = .{ .min = overlap_region.point_min.x, .max = overlap_region.point_max.x };
        xs_len += 1;
        if (overlap_region.point_min.x > self.point_min.x) {
            xs[xs_len] = .{ .min = self.point_min.x, .max = overlap_region.point_min.x - 1 };
            xs_len += 1;
        }
        if (overlap_region.point_max.x < self.point_max.x) {
            xs[xs_len] = .{ .min = overlap_region.point_max.x + 1, .max = self.point_max.x };
            xs_len += 1;
        }

        ys[0] = .{ .min = overlap_region.point_min.y, .max = overlap_region.point_max.y };
        ys_len += 1;
        if (overlap_region.point_min.y > self.point_min.y) {
            ys[ys_len] = .{ .min = self.point_min.y, .max = overlap_region.point_min.y - 1 };
            ys_len += 1;
        }
        if (overlap_region.point_max.y < self.point_max.y) {
            ys[ys_len] = .{ .min = overlap_region.point_max.y + 1, .max = self.point_max.y };
            ys_len += 1;
        }

        zs[0] = .{ .min = overlap_region.point_min.z, .max = overlap_region.point_max.z };
        zs_len += 1;
        if (overlap_region.point_min.z > self.point_min.z) {
            zs[zs_len] = .{ .min = self.point_min.z, .max = overlap_region.point_min.z - 1 };
            zs_len += 1;
        }
        if (overlap_region.point_max.z < self.point_max.z) {
            zs[zs_len] = .{ .min = overlap_region.point_max.z + 1, .max = self.point_max.z };
            zs_len += 1;
        }

        var xs_it: usize = 0;
        while (xs_it < xs_len) : (xs_it += 1) {
            var ys_it: usize = 0;
            while (ys_it < ys_len) : (ys_it += 1) {
                var zs_it: usize = 0;
                while (zs_it < zs_len) : (zs_it += 1) {
                    const xx = xs[xs_it];
                    const yy = ys[ys_it];
                    const zz = zs[zs_it];
                    const subcuboid = Cuboid{
                        .point_min = .{ .x = xx.min, .y = yy.min, .z = zz.min },
                        .point_max = .{ .x = xx.max, .y = yy.max, .z = zz.max },
                    };
                    if (!overlap_region.overlaps(subcuboid)) try cuboids.append(subcuboid);
                }
            }
        }

        return cuboids.toOwnedSlice();
    }
};

test "cuboid overlaps" {
    const c1 = Cuboid{
        .point_min = .{ .x = 10, .y = 10, .z = 10 },
        .point_max = .{ .x = 12, .y = 12, .z = 12 },
    };
    const c2 = Cuboid{
        .point_min = .{ .x = 11, .y = 11, .z = 11 },
        .point_max = .{ .x = 13, .y = 13, .z = 13 },
    };
    try testing.expect(c1.overlaps(c2));

    const c3 = c1.overlappingRegion(c2);
    try testing.expect(c3.point_min.x == 11);
    try testing.expect(c3.point_min.y == 11);
    try testing.expect(c3.point_min.y == 11);
    try testing.expect(c3.point_max.x == 12);
    try testing.expect(c3.point_max.y == 12);
    try testing.expect(c3.point_max.y == 12);
}

test "cuboid volume" {
    const c = Cuboid{
        .point_min = .{ .x = 11, .y = 11, .z = 11 },
        .point_max = .{ .x = 12, .y = 12, .z = 12 },
    };
    try testing.expect(c.volume() == 8);
}

const RebootStep = struct {
    on: bool,
    region: Cuboid,
};
