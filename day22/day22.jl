using GLMakie

const Point3i = Point{3, Int}

struct Cuboid
  pointMin::Point3i
  pointMax::Point3i
end

function toRect3i(cuboid::Cuboid)
  dimensions = cuboid.pointMax - cuboid.pointMin + Point3i(1, 1, 1)
  return Rect3i(cuboid.pointMin, dimensions)
end

function volume(c::Cuboid)
  diff = c.pointMax - c.pointMin + Point3i(1, 1, 1)
  vol = reduce(*, diff)
  return vol
end

function contain(c1::Cuboid, c2::Cuboid)
  contain_x = c1.pointMin.data[1] <= c2.pointMin.data[1] &&
              c1.pointMax.data[1] >= c2.pointMax.data[1]
  contain_y = c1.pointMin.data[2] <= c2.pointMin.data[2] &&
              c1.pointMax.data[2] >= c2.pointMax.data[2]
  contain_z = c1.pointMin.data[3] <= c2.pointMin.data[3] &&
              c1.pointMax.data[3] >= c2.pointMax.data[3]
  contained = contain_x && contain_y && contain_z
  return contained
end

function overlap(c1::Cuboid, c2::Cuboid)
  overlap_x = c1.pointMin.data[1] <= c2.pointMax.data[1] &&
              c1.pointMax.data[1] >= c2.pointMin.data[1]
  overlap_y = c1.pointMin.data[2] <= c2.pointMax.data[2] &&
              c1.pointMax.data[2] >= c2.pointMin.data[2]
  overlap_z = c1.pointMin.data[3] <= c2.pointMax.data[3] &&
              c1.pointMax.data[3] >= c2.pointMin.data[3]
  overlapped = overlap_x && overlap_y && overlap_z
  return overlapped
end

function overlapRegion(c1::Cuboid, c2::Cuboid)
  pointMin = Point3i(max(c1.pointMin.data[1], c2.pointMin.data[1]),
                     max(c1.pointMin.data[2], c2.pointMin.data[2]),
                     max(c1.pointMin.data[3], c2.pointMin.data[3]))
  pointMax = Point3i(min(c1.pointMax.data[1], c2.pointMax.data[1]),
                     min(c1.pointMax.data[2], c2.pointMax.data[2]),
                     min(c1.pointMax.data[3], c2.pointMax.data[3]))
  region = Cuboid(pointMin, pointMax)
  return region
end

function removeRegion(cuboid::Cuboid, region::Cuboid)
  xranges = Tuple{Float32, Float32}[]
  push!(xranges, (region.pointMin.data[1], region.pointMax.data[1]))
  if region.pointMin.data[1] > cuboid.pointMin.data[1]
    push!(xranges, (cuboid.pointMin.data[1], region.pointMin.data[1] - 1))
  end
  if region.pointMax.data[1] < cuboid.pointMax.data[1]
    push!(xranges, (region.pointMax.data[1] + 1, cuboid.pointMax.data[1]))
  end

  yranges = Tuple{Float32, Float32}[]
  push!(yranges, (region.pointMin.data[2], region.pointMax.data[2]))
  if region.pointMin.data[2] > cuboid.pointMin.data[2]
    push!(yranges, (cuboid.pointMin.data[2], region.pointMin.data[2] - 1))
  end
  if region.pointMax.data[2] < cuboid.pointMax.data[2]
    push!(yranges, (region.pointMax.data[2] + 1, cuboid.pointMax.data[2]))
  end

  zranges = Tuple{Float32, Float32}[]
  push!(zranges, (region.pointMin.data[3], region.pointMax.data[3]))
  if region.pointMin.data[3] > cuboid.pointMin.data[3]
    push!(zranges, (cuboid.pointMin.data[3], region.pointMin.data[3] - 1))
  end
  if region.pointMax.data[3] < cuboid.pointMax.data[3]
    push!(zranges, (region.pointMax.data[3] + 1, cuboid.pointMax.data[3]))
  end

  subcuboids = Cuboid[]

  for xx in xranges, yy in yranges, zz in zranges
    isInCuboid = xx[1] >= cuboid.pointMin.data[1] &&
                 xx[2] <= cuboid.pointMax.data[1] &&
                 yy[1] >= cuboid.pointMin.data[2] &&
                 yy[2] <= cuboid.pointMax.data[2] &&
                 zz[1] >= cuboid.pointMin.data[3] &&
                 zz[2] <= cuboid.pointMax.data[3]
    isInRegion = xx[1] >= region.pointMin.data[1] &&
                 xx[2] <= region.pointMax.data[1] &&
                 yy[1] >= region.pointMin.data[2] &&
                 yy[2] <= region.pointMax.data[2] &&
                 zz[1] >= region.pointMin.data[3] &&
                 zz[2] <= region.pointMax.data[3]

    if !isInRegion
      subcuboid = Cuboid(Point3i(xx[1], yy[1], zz[1]),
                         Point3i(xx[2], yy[2], zz[2]))
      push!(subcuboids,subcuboid)
    end
  end

  return subcuboids
end

struct RebootStep
  on::Bool
  region::Cuboid
end

function parseInput(data)
  steps = RebootStep[]
  for line in split(data, '\n')
    if length(line) == 0
      break
    end

    state, cuboidRegion = split(line, ' ')
    xrange, yrange, zrange = split(cuboidRegion, ',') .|>
                             (x -> split(x, '=')[2])

    xmin, xmax = split(xrange, "..") .|> (x -> parse(Int, x))
    ymin, ymax = split(yrange, "..") .|> (x -> parse(Int, x))
    zmin, zmax = split(zrange, "..") .|> (x -> parse(Int, x))

    on = if state == "on"
      true
    elseif state == "off"
      false
    else
      throw("Invalid boot status")
    end

    step = RebootStep(on, Cuboid(Point3i(xmin, ymin, zmin),
                                 Point3i(xmax, ymax, zmax)))
    push!(steps, step)
  end
  return steps
end

function reboot(steps::Vector{RebootStep})
  nextSteps = collect(reverse(steps))
  nonoverlapRegions = Cuboid[]

  while !isempty(nextSteps)
    step = pop!(nextSteps)
    hasOverlap = false
    for (regionIdx, nonoverlapRegion) in enumerate(nonoverlapRegions)
      if contain(nonoverlapRegion, step.region)
        if !step.on
          nonoverlapPartitions = removeRegion(nonoverlapRegion, step.region)
          deleteat!(nonoverlapRegions, regionIdx)
          append!(nonoverlapRegions, nonoverlapPartitions)
        end
        hasOverlap = true
        break
      elseif contain(step.region, nonoverlapRegion)
        deleteat!(nonoverlapRegions, regionIdx)
        push!(nextSteps, step)
        hasOverlap = true
        break
      elseif overlap(step.region, nonoverlapRegion)
        sharedRegion = overlapRegion(step.region, nonoverlapRegion)
        if step.on
          nonoverlapPartitions = removeRegion(step.region, sharedRegion)
          splitSteps = map(x -> RebootStep(step.on, x), nonoverlapPartitions)
          append!(nextSteps, splitSteps)
        else
          nonoverlapPartitions = removeRegion(nonoverlapRegion, sharedRegion)
          deleteat!(nonoverlapRegions, regionIdx)
          append!(nonoverlapRegions, nonoverlapPartitions)
          push!(nextSteps, step)
        end
        hasOverlap = true
        break
      end
    end

    if step.on && !hasOverlap
      push!(nonoverlapRegions, step.region)
    end
  end

  return nonoverlapRegions
end

function viz(steps::Vector{RebootStep}, nonoverlaps::Vector{Cuboid})
  fig = Figure()
  ax1 = Axis3(fig[1, 1])
  ax2 = Axis3(fig[1, 2])

  for step in steps
    color = if step.on
      :green
    else
      :red
    end
    wireframe!(ax1, toRect3i(step.region); color)
  end

  for region in nonoverlaps
    wireframe!(ax2, toRect3i(region))
  end

  return fig
end

function reboot(data, region=nothing; visualization=false)
  rebootSteps = parseInput(data)
  if !isnothing(region)
    filter!(x -> contain(region, x.region), rebootSteps)
  end
  nonoverlaps = reboot(rebootSteps)

  vol = 0
  for cuboid in nonoverlaps
    vol += volume(cuboid)
  end

  fig = nothing
  if visualization
    fig = viz(rebootSteps, nonoverlaps)
  end

  return vol, nonoverlaps, fig
end

let
  v1, _, _ = reboot(read("input/day22.txt", String),
                 Cuboid(Point3i(-50, -50, -50), Point3i(50, 50, 50)),
                 visualization=false)
  v2, _, _ = reboot(read("input/day22.txt", String),
                 visualization=false)
  println("Part 1 $v1 | Part 2 $v2")
end

