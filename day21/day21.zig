const std = @import("std");
const debug = std.debug;
const fmt = std.fmt;
const heap = std.heap;
const math = std.math;
const mem = std.mem;
const io = std.io;
const testing = std.testing;

const BUFFER_SIZE = 64;

pub fn main() !void {
    var gpa = heap.GeneralPurposeAllocator(.{}){};
    defer debug.assert(!gpa.deinit());

    var buf: [BUFFER_SIZE]u8 = undefined;
    var reader = io.getStdIn().reader();

    var players: [2]usize = undefined;
    var i: usize = 0;
    while (i < 2) : (i += 1) {
        const line = (try reader.readUntilDelimiterOrEof(&buf, '\n')).?;
        var elems = mem.split(u8, line, ": ");
        _ = elems.next().?;
        players[i] = try fmt.parseInt(usize, elems.next().?, 10);
    }

    var ans1 = part01(players);
    debug.print("part 1: {}\n", .{ans1});
    var ans2 = part02(players);
    debug.print("part 2: {}\n", .{ans2});
}

fn newPos(starting_pos: usize, steps: usize) usize {
    const s = (starting_pos + steps - 1) % 10 + 1;
    return s;
}

fn part01(players_: [2]usize) usize {
    var players = players_;
    var scores = [_]usize{ 0, 0 };
    var moves: usize = 1;
    while (scores[0] < 1000 and scores[1] < 1000) : (moves += 1) {
        const current_player = (moves - 1) % 2;
        const steps = 9 * (moves - 1) + 6;
        players[current_player] = newPos(players[current_player], steps);
        scores[current_player] += players[current_player];
    }

    debug.print("moves: {} | scores: {any}\n", .{ moves, scores });
    var score_loser = math.min(scores[0], scores[1]);
    var ans = 3 * (moves - 1) * score_loser;
    return ans;
}

fn part02(positions: [2]usize) !usize {
    // Each player takes 3 turns, which mean that all possible 3-dice
    // combinations are encountered. As a result, there're always:
    // + 1 world moves 3 steps
    // + 3 worlds move 4 steps
    // + 6 worlds move 5 steps
    // + 7 worlds move 6 steps
    // + 6 worlds move 7 steps
    // + 3 worlds move 8 steps
    // + 1 world moves 9 steps
    // => We can used a hash map to keep count of how many worlds are
    // currently having the same scores and positions, then the number
    // of new worlds will be the number of worlds with the current state
    // times the number of worlds that can be created given the number of
    // steps

    var gpa = heap.GeneralPurposeAllocator(.{}){};
    defer debug.assert(!gpa.deinit());

    const MAX_SCORE = 21;

    // a world contains the current positions and scores of two players
    const World = struct { positions: [2]usize, scores: [2]usize };

    // this keep count of how many worlds there are
    var worlds = std.AutoHashMap(World, usize).init(gpa.allocator());
    defer worlds.deinit();
    try worlds.put(.{ .scores = .{ 0, 0 }, .positions = positions }, 1);

    // this keeps count of the newly created worlds
    var new_worlds = std.AutoHashMap(World, usize).init(gpa.allocator());
    defer new_worlds.deinit();

    var current_player: usize = 0;
    while (true) {
        var finished = true;

        // go though all the current existing worlds and split each world
        // into new ones based on the number of steps that can be taken
        var worlds_it = worlds.iterator();
        while (worlds_it.next()) |world_entry| {
            const world = world_entry.key_ptr.*;
            const count = world_entry.value_ptr.*;

            // only split if the maximum possible score is not reached
            if (math.max(world.scores[0], world.scores[1]) < MAX_SCORE) {
                // go through all possible results of rolling a 3-face dice
                // 3 times
                var steps: usize = 3;
                while (steps <= 9) : (steps += 1) {
                    const current_position = world.positions[current_player];
                    const current_score = world.scores[current_player];

                    const new_position = newPos(current_position, steps);
                    const new_score = current_score + new_position;

                    var new_world = world;
                    new_world.scores[current_player] = new_score;
                    new_world.positions[current_player] = new_position;

                    const new_count = switch (steps) {
                        3 => count * 1,
                        4 => count * 3,
                        5 => count * 6,
                        6 => count * 7,
                        7 => count * 6,
                        8 => count * 3,
                        9 => count * 1,
                        else => unreachable,
                    };

                    var new_world_entry = try new_worlds.getOrPut(new_world);
                    if (new_world_entry.found_existing) {
                        new_world_entry.value_ptr.* += new_count;
                    } else {
                        new_world_entry.value_ptr.* = new_count;
                    }
                }
                finished = false;
            } else {
                var new_world_entry = try new_worlds.getOrPut(world);
                if (new_world_entry.found_existing) {
                    new_world_entry.value_ptr.* += count;
                } else {
                    new_world_entry.value_ptr.* = count;
                }
            }
        }

        if (finished) break;

        mem.swap(std.AutoHashMap(World, usize), &worlds, &new_worlds);
        new_worlds.clearRetainingCapacity();

        current_player += 1;
        current_player %= 2;
    }

    var wins = [_]usize{ 0, 0 };
    var worlds_it = worlds.iterator();
    while (worlds_it.next()) |world_entry| {
        const world = world_entry.key_ptr.*;
        const count = world_entry.value_ptr.*;
        if (world.scores[0] >= MAX_SCORE) wins[0] += count;
        if (world.scores[1] >= MAX_SCORE) wins[1] += count;
    }

    const winner = math.max(wins[0], wins[1]);
    return winner;
}
