const std = @import("std");
const debug = std.debug;
const math = std.math;
const mem = std.mem;
const io = std.io;
const heap = std.heap;

const BUFFER_CAPACITY = 512;

pub fn main() !void {
    var gpa = heap.GeneralPurposeAllocator(.{}){};
    defer debug.assert(!gpa.deinit());

    var buf: [BUFFER_CAPACITY]u8 = undefined;
    var reader = io.getStdIn().reader();

    var algorithm: [512]bool = undefined;
    const algorithm_line = (try reader.readUntilDelimiterOrEof(&buf, '\n')).?;
    debug.assert(algorithm_line.len == 512);
    for (algorithm_line) |c, i| {
        switch (c) {
            '.' => algorithm[i] = false,
            '#' => algorithm[i] = true,
            else => return error.BadInput,
        }
    }
    _ = (try reader.readUntilDelimiterOrEof(&buf, '\n')).?;

    var pixels = std.ArrayList([]bool).init(gpa.allocator());
    defer {
        for (pixels.items) |row| gpa.allocator().free(row);
        pixels.deinit();
    }

    while (reader.readUntilDelimiterOrEof(&buf, '\n')) |maybe_line| {
        const line = maybe_line orelse break;
        if (line.len == 0) continue;

        var pixels_row = std.ArrayList(bool).init(gpa.allocator());
        defer pixels_row.deinit();

        for (line) |c| {
            switch (c) {
                '.' => try pixels_row.append(false),
                '#' => try pixels_row.append(true),
                else => return error.BadInput,
            }
        }
        try pixels.append(pixels_row.toOwnedSlice());
    } else |err| return err;

    var image1 = try Image.init(
        gpa.allocator(),
        pixels.items,
    );
    defer image1.deinit();

    try image1.enhance(algorithm, 2);
    debug.print("number of light pixels: {}\n", .{image1.light_pixels.count()});

    var image2 = try Image.init(
        gpa.allocator(),
        pixels.items,
    );
    defer image2.deinit();

    try image2.enhance(algorithm, 50);
    debug.print("number of light pixels: {}\n", .{image2.light_pixels.count()});
}

const Pos = struct {
    const Self = @This();
    x: isize,
    y: isize,

    fn add(lhs: Self, rhs: Self) Self {
        return .{
            .x = lhs.x + rhs.x,
            .y = lhs.y + rhs.y,
        };
    }
};

// We only care about the area that contains the original input image.
// All other pixels in the infinitely-sized space are homogeneous, so
// we don't have to save them in the struct.
const Image = struct {
    const Self = @This();
    xmin: isize,
    ymin: isize,
    xmax: isize,
    ymax: isize,
    is_inf_light: bool,
    light_pixels: std.AutoHashMap(Pos, void),

    fn init(allocator: mem.Allocator, pixels: [][]bool) !Self {
        debug.assert(pixels.len != 0);

        var light_pixels = std.AutoHashMap(Pos, void).init(allocator);
        for (pixels) |row, y| {
            for (row) |p, x| if (p) try light_pixels.put(.{
                .x = @intCast(isize, x),
                .y = @intCast(isize, y),
            }, {});
        }

        const image = .{
            .xmin = 0,
            .ymin = 0,
            .xmax = @intCast(isize, pixels[0].len - 1),
            .ymax = @intCast(isize, pixels.len - 1),
            .is_inf_light = false,
            .light_pixels = light_pixels,
        };

        return image;
    }

    fn deinit(self: *Self) void {
        self.light_pixels.deinit();
    }

    fn enhance(self: *Self, algorithm: [512]bool, n: usize) !void {
        const ORDERED_OFFSETS = [_]Pos{
            .{ .x = -1, .y = -1 },
            .{ .x = 0, .y = -1 },
            .{ .x = 1, .y = -1 },
            .{ .x = -1, .y = 0 },
            .{ .x = 0, .y = 0 },
            .{ .x = 1, .y = 0 },
            .{ .x = -1, .y = 1 },
            .{ .x = 0, .y = 1 },
            .{ .x = 1, .y = 1 },
        };

        var gpa = heap.GeneralPurposeAllocator(.{}){};
        defer debug.assert(!gpa.deinit());

        var off_pixels = std.ArrayList(Pos).init(gpa.allocator());
        defer off_pixels.deinit();

        var on_pixels = std.ArrayList(Pos).init(gpa.allocator());
        defer on_pixels.deinit();

        // do `n` enhancements
        var t: usize = 0;
        while (t < n) : (t += 1) {
            var y: isize = self.ymin - 2;
            while (y <= self.ymax + 2) : (y += 1) {
                var x: isize = self.xmin - 2;
                while (x <= self.xmax + 2) : (x += 1) {
                    const pixel = Pos{ .x = x, .y = y };
                    var algorithm_idx: usize = 0;
                    for (ORDERED_OFFSETS) |offset| {
                        const nearby_pixel = pixel.add(offset);
                        algorithm_idx *= 2;
                        if (self.light_pixels.contains(nearby_pixel) or
                            (self.is_inf_light and
                            (nearby_pixel.x < self.xmin or
                            nearby_pixel.x > self.xmax or
                            nearby_pixel.y < self.ymin or
                            nearby_pixel.y > self.ymax)))
                        {
                            algorithm_idx += 1;
                        }
                    }
                    if (algorithm[algorithm_idx])
                        try on_pixels.append(pixel)
                    else
                        try off_pixels.append(pixel);
                }
            }

            self.xmin -= 2;
            self.ymin -= 2;
            self.xmax += 2;
            self.ymax += 2;

            self.is_inf_light = if (self.is_inf_light) algorithm[511] else algorithm[0];

            for (on_pixels.items) |pixel| try self.light_pixels.put(pixel, {});
            for (off_pixels.items) |pixel| _ = self.light_pixels.remove(pixel);
            on_pixels.clearRetainingCapacity();
            off_pixels.clearRetainingCapacity();
        }
    }

    fn print(self: Self) void {
        var y: isize = self.ymin;
        while (y <= self.ymax) : (y += 1) {
            var x: isize = self.xmin;
            while (x <= self.xmax) : (x += 1) {
                const pixel = Pos{ .x = x, .y = y };
                if (self.light_pixels.contains(pixel))
                    debug.print("#", .{})
                else
                    debug.print(".", .{});
            }
            debug.print("\n", .{});
        }
    }
};
